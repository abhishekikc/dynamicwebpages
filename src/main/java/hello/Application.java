package hello;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.SpringVersion;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
import org.springframework.test.context.web.WebAppConfiguration;

//@EnableHypermediaSupport(type = { HypermediaType.HAL })
@WebAppConfiguration
@SpringBootApplication
public class Application implements CommandLineRunner{
	@Autowired
	CustomerRepository repository;
	@Autowired
	CustomerObjectRepository customerObjectRepository;
	 
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
        System.out.println("version: " + SpringVersion.getVersion());
        
    }
    
    @Override
    public void run(String... strings) throws Exception {
    	// fetch all customers
        System.out.println("Customers found with findAll():");
        System.out.println("-------------------------------");
        for (Customer customer : repository.findAll()) {
            System.out.println(customer);
        }
        System.out.println();
       
        // fetch an individual customer by ID
        Customer customer = repository.findOne(1L);
        System.out.println("Customer found with findOne(1L):");
        System.out.println("--------------------------------");
        System.out.println(customer);
        System.out.println();
        
        
        // fetch customers by last name
        
        System.out.println("Customer found with findByLastName('Tiwari'):");
        System.out.println("--------------------------------------------");
        for (Customer bauer : repository.findByLastName("Tiwari")) {
            System.out.println(bauer);
            
        }
        
        System.out.println("--------------------------------------------");
        System.out.println("Customer in sorted format:");
        System.out.println("--------------------------------------------");
        List<Customer> sorted = repository.findAllByOrderByFirstNameDesc();
        
        //overWriteRecord();
        for(Customer palmer : sorted){
        	System.out.println(palmer);
        	
        }
        
     // fetch an individual customer by ID
        CustomerObject customerObject = customerObjectRepository.findOne(5L);
        System.out.println("Customer found with findOne(5L):");
        System.out.println("--------------------------------");
        System.out.println(customerObject.getObjectName());
        System.out.println();
        
        
        
        
    }

}
