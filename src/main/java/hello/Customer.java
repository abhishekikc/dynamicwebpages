package hello;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="customer", schema="customerSchema")
public class Customer {
	
    @Id
    @Column(name = "customer_id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long customerId;
    private String firstName;
    private String lastName;
    
    @OneToMany(mappedBy="customer")
    @JsonManagedReference("CustomerToObject")
    private List<CustomerObject> customerObjectList;
    
    protected Customer() {}
    
	public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
        
    public List<CustomerObject> getCustomerObjectList() {
		return customerObjectList;
	}

	public void setCustomerObjectList(List<CustomerObject> customerObjectList) {
		this.customerObjectList = customerObjectList;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
    @Override
    public String toString() {
    	return String.format(
        		"Customer[id=%d, firstName='%s', lastName='%s']"
        		,customerId, firstName, lastName);
    }
	
}

