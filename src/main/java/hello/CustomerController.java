package hello;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {
	
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
    private CustomerResourceAssembler customerResourceAssembler;
	
	@RequestMapping(value="/customer",method = RequestMethod.GET, produces = {"application/json"})
    public PagedResources<Customer> greeting(Pageable pageable ,PagedResourcesAssembler assembler){
    	
    	//Pageable pageable = new PageRequest(1, 3, Direction.ASC, "firstName");
        
        Page<Customer> customers = customerRepository.findAll(pageable);
        
        return assembler.toResource(customers, customerResourceAssembler);
    }
    
}
