package hello;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="customer_object", schema="customerSchema")
public class CustomerObject {
	@Id
    @Column(name = "customer_obj_id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long customerObjId;
	
	private int objectNum;
	private String objectName;
	
	@ManyToOne
	@JoinColumn(name="customer_id")
	@JsonBackReference("CustomerToObject")
	private Customer customer;
	
	protected CustomerObject() {}
	
	public int getObjectNum() {
		return objectNum;
	}

	public CustomerObject(int objectNum, String objectName, Customer customer) {
		super();
		this.objectNum = objectNum;
		this.objectName = objectName;
		this.customer = customer;
	}

	public long getCustomerObjId() {
		return customerObjId;
	}

	public void setCustomerObjId(long customerObjId) {
		this.customerObjId = customerObjId;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setObjectNum(int objectNum) {
		this.objectNum = objectNum;
	}


}
