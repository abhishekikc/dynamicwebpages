package hello;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	@Transactional
	public Page<Customer> findAll(Pageable pageable);
	public List<Customer> findAll();
    
    public List<Customer> findByLastName(String lastName);
    public List<Customer> findAllByOrderByFirstNameDesc();
    //List<CustomerObject> findCustomerObjectByCustomerObject_ObjectNum(int objectNum);
}
