package hello;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class CustomerResourceAssembler extends ResourceAssemblerSupport<Customer,Resource>{
	
	public CustomerResourceAssembler() {
		super(CustomerController.class,Resource.class);
	}
	
	@Override
    public List<Resource> toResources(Iterable<? extends Customer> customers) {
        List<Resource> resources = new ArrayList<Resource>();
        for(Customer customer : customers) {
            resources.add(new Resource<Customer>(customer, linkTo(CustomerController.class).slash(customer.getCustomerId()).withSelfRel()));
        }
        System.out.println("Resource assembler workin fine.");
        return resources;
    }
 
    @Override
    public Resource toResource(Customer customer) {
        return new Resource<Customer>(customer, linkTo(CustomerController.class).slash(customer.getCustomerId()).withSelfRel());
    }

}
