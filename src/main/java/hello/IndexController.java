package hello;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {
	
	@Autowired
	CustomerRepository customerRepository;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String indexPage(Model model) {
		List<Customer> customers = customerRepository.findAll();
		//model.addAttribute("customers",customers);
		return "index";
	}
}
